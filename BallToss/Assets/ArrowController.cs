﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
        
    {
        if(Input.GetKey(KeyCode.LeftArrow))　//左矢印キーが押されたら
        {
            transform.Rotate(0,0,-3);　//左に3動かす
        }

        if (Input.GetKey(KeyCode.RightArrow))　//右矢印キーが押されたら
        {
            transform.Rotate(0, 0,3);  //右に3動かす
        }
    }
}
