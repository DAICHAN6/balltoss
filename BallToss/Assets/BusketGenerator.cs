﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusketGenerator : MonoBehaviour
{
    public GameObject BasketPrefab;
    float span = 1.0f;　//長さ
    float delta = 0;  //間隔

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;  //時間を追加
        if(this.delta>this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(BasketPrefab) as GameObject;
            go.transform.position = new Vector3(-10, -3, 0);
        }
    }
}
