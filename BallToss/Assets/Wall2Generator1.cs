﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall2Generator1 : MonoBehaviour
{
    public GameObject WallPrefab1;
    float span = 1.0f;　//長さ
    float delta = 3;  //間隔

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;  //時間を追加
        if (this.delta > this.span)
        {
            this.delta = 0;
            int px = Random.Range(1, -2);
            GameObject go = Instantiate(WallPrefab1) as GameObject;
            go.transform.position = new Vector3(10, px, 0);
        }
    }
}
