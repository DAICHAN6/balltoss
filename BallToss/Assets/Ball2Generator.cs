﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball2Generator : MonoBehaviour
{
    public GameObject BallPrefab;

    // Start is called before the first frame update
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {
        float time = GameDirector2.time;

        if (time > 0)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                GameObject ball = Instantiate(BallPrefab) as GameObject;
                ball.transform.position = transform.position;
                ball.transform.rotation = transform.rotation;
            }
        }
    }
}

