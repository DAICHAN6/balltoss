﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    public GameObject BallPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    
    // Update is called once per frame
    void Update()
    {
        float time = GameDirector.time;

        if (time > 0)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                GameObject ball = Instantiate(BallPrefab) as GameObject;
                ball.transform.position = transform.position;
                ball.transform.rotation = transform.rotation;
            }
        }
    }
}
