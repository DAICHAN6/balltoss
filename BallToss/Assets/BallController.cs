﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    Rigidbody2D rigid2D;
    float BoundForce = 280.0f;

    // Start is called before the first frame update
    void Start()
    {
        this.rigid2D = GetComponent<Rigidbody2D>();
        this.rigid2D.AddForce(transform.up * this.BoundForce);
    }

   
    // Update is called once per frame
    void Update()
    {
        if(transform.position.y<-10.0f)  //画面外に出たらオブジェクトを破棄
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("ゴール");
        Destroy(gameObject);

        GameObject Director = GameObject.Find("GameDirector");
        Director.GetComponent<GameDirector>().DecreaseCo();
    }
}
