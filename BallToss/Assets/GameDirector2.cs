﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector2 : MonoBehaviour
{

    //他のスクリプトに移せる変数

    public static int Co = 0;
    public static float time = 1800.0f;
    public GameObject BallCount2 = null;  //0を意味する
    GameObject timer;

    // Start is called before the first frame update
    void Start()
    {
        this.timer = GameObject.Find("timer"); //探す
        time = 15.0f; //初期時間
        Co = 0;
    }

    public void DecreaseCo()
    {
        Co++;  //スコア増やす
    }

    // Update is called once per frame
    void Update()
    {
        Text arrowText = this.BallCount2.GetComponent<Text>();
        arrowText.text = Co + "個";  //スコア表示

        time -= Time.deltaTime;
        this.timer.GetComponent<Image>().fillAmount = time / 15.0f; //タイムリミット
    }
}
